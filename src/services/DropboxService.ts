import { Dropbox } from 'dropbox';
import { IAddDocumentEvent } from '../components/Documents/AddDocument/model';

class DropboxService {
  private static CLIENT_ID = 'xkwwi99exiia63d';
  private session: Dropbox;
  private accessToken: string | null;

  constructor() {
    this.session = new Dropbox({ clientId: DropboxService.CLIENT_ID });

    this.accessToken = localStorage.getItem('accessToken') || this.fetchAccessTokenFromUrl();
    localStorage.setItem('accessToken', this.accessToken);
    this.session.setAccessToken(this.accessToken);
  }

  public isAuthenticated(): boolean {
    return !!this.accessToken;
  }

  public getAuthUrl() {
    return this.session.getAuthenticationUrl('http://localhost:3000/auth');
  }

  public fetchAccessTokenFromUrl(): string {
    const query = window.location.hash;

    console.log(window.location);
    const accessTokenPart = query
      .substring(1)
      .split('&')
      .find(part => part.startsWith('access_token'));
    return accessTokenPart ? accessTokenPart.replace('access_token=', '') : '';
  }

  public getFileNames() {
    return this.session.filesListFolder({ path: '' });
  }

  public uploadDocument(event: IAddDocumentEvent) {
    console.log(event);
    return this.session.filesUpload({ path: `/${event.title}.pdf`, contents: event.file });
  }
}

export default new DropboxService();
