import * as React from 'react';
import dropbox from '../../assets/dropbox.svg';
import './MtAuthentication.css';

import dropboxService from '../../services/DropboxService';

class MtAuthentication extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = { loading: true };
  }

  public componentDidMount() {
    if (dropboxService.isAuthenticated()) {
      this.props.history.replace('/');
    } else {
      this.setState({ ...this.state, loading: false, authUrl: dropboxService.getAuthUrl() });
    }
  }

  public render() {
    return (
      <section hidden={this.state.loading} className="text-center mt-5">
        <h4>Hi there! Mithril requires access to Dropbox to securely store your documents. Hit the button below to login.</h4>
        <a role="button" className="button" href={this.state.authUrl}>
          <div className="button__content p-05">
            <img src={dropbox} className="mr-05" />
            Login with Dropbox
          </div>
        </a>
      </section>
    );
  }
}

export default MtAuthentication;
