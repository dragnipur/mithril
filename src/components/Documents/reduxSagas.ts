import { all, call, put, takeEvery } from 'redux-saga/effects';
import { ADD_DOCUMENT, FETCH_DOCUMENTS, STORE_DOCUMENTS } from './reduxActionTypes';

import dropboxService from '../../services/DropboxService';
import { IAddDocumentEvent } from './AddDocument/model';
import { IDocument } from './model';

export function* fetchDocuments() {
  const response = yield call(() => dropboxService.getFileNames());
  const documents: IDocument[] =
    response.entries.map((file: any, index: number) => {
      return { id: String(index), title: file.name };
    }) || [];
    
  yield put({ type: STORE_DOCUMENTS, payload: documents });
}

export function* addDocument(event: any) {
  const payload: IAddDocumentEvent = event.payload;
  yield dropboxService.uploadDocument(payload);
  yield put({ type: FETCH_DOCUMENTS });
}

export default function* sagas() {
  yield all([
    yield takeEvery(FETCH_DOCUMENTS, fetchDocuments),
    yield takeEvery(ADD_DOCUMENT as any, addDocument)
  ]);

}
