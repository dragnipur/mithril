import * as React from 'react';

import './MtDocuments.css';

import { connect } from 'react-redux';
import MtAddDocument from './AddDocument/AddDocument';
import { IAddDocumentEvent } from './AddDocument/model';
import { IDocument } from './model';
import { ADD_DOCUMENT, FETCH_DOCUMENTS } from './reduxActionTypes';

export interface IDocumentsState {
  isAddOpen: boolean;
}

class MtDocuments extends React.Component<any, IDocumentsState> {
  constructor(props: {}) {
    super(props);
    this.state = { isAddOpen: false };
  }

  public async componentDidMount() {
    this.props.dispatch({ type: FETCH_DOCUMENTS });
  }

  public render() {
    return (
      <section className="documents">
        <aside className="documents__sidenav hidden-mobile">
          <h1 className="ml-1 mt-1 mr-2">Documents</h1>
          <div>
            <div className="documents__category p-1">All documents</div>
          </div>
        </aside>

        <div className="documents__content p-1">
          {this.props.documents.map((document: IDocument) => {
            return (
              <section className="document-card m-2 p-1" key={document.id}>
                <header className="document-card__header">
                  <h2 className="document-card__title">{document.title}</h2>
                  <button className="fab small ripple">...</button>
                </header>

                <div>
                  <div>unsigned</div>
                  <div>signed</div>
                </div>
              </section>
            );
          })}
        </div>
        <MtAddDocument
          isOpen={this.state.isAddOpen}
          title="Add a new document"
          onClose={this.onCloseAddDocument}
          onSave={this.onSaveDocument}
        />

        <button className="fab large ripple documents__add mb-1" onClick={this.onOpenAddDocument}>
          <span>+</span>
        </button>
      </section>
    );
  }

  private onOpenAddDocument = () => {
    this.setState({ ...this.state, isAddOpen: true });
  };

  private onCloseAddDocument = () => {
    this.setState({ ...this.state, isAddOpen: false });
  };

  private onSaveDocument = async (data: IAddDocumentEvent) => {
    this.props.dispatch({ type: ADD_DOCUMENT, payload: data });
    this.onCloseAddDocument();
  };
}

const mapStateToProps = (state: any) => {
  return {
    documents: state.documentsReducers.documents
  };
};

export default connect(mapStateToProps)(MtDocuments);
