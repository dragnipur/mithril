import { Action, handleAction } from 'redux-actions';

import { IDocument } from './model';
import { STORE_DOCUMENTS } from './reduxActionTypes';

interface IState {
  documents: IDocument[];
}

const initialState: IState = {
  documents: []
};

export default handleAction<IState, IDocument[]>(
  STORE_DOCUMENTS,
  (state: IState, action: Action<IDocument[]>): IState => {
    return {
      documents: action.payload || []
    };
  },
  initialState
);
