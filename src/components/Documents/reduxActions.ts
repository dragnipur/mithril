import { createAction } from 'redux-actions';

import { IAddDocumentEvent } from './AddDocument/model';
import { IDocument } from './model';
import { ADD_DOCUMENT, FETCH_DOCUMENTS } from './reduxActionTypes';

const fetchDocuments = createAction<IDocument[]>(FETCH_DOCUMENTS);
const addDocument = createAction(ADD_DOCUMENT, (document: IAddDocumentEvent) => document);
export { fetchDocuments, addDocument };
