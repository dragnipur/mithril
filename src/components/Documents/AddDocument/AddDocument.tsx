import { Formik } from 'formik';
import * as React from 'react';

import MtModal from '../../Core/Modal/MtModal';
import './AddDocument.css';

import { IMtModalProps } from '../../Core/Modal/model';
import { IAddDocumentEvent } from './model';

export interface IMtAddDocumentProps extends IMtModalProps {
  onSave: (e: IAddDocumentEvent) => void;
}

class MtAddDocument extends React.Component<IMtAddDocumentProps, any> {
  private documentUpload: HTMLInputElement | null;

  public render() {
    return (
      <MtModal isOpen={this.props.isOpen} title="Add a new document" onClose={this.props.onClose}>
        <Formik
          initialValues={{
            file: '',
            title: ''
          }}
          validate={this.validateForm}
          onSubmit={this.onSubmitForm}
          render={this.renderForm}
        />
      </MtModal>
    );
  }

  private renderForm = ({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }: any) => {
    return (
      <form onSubmit={handleSubmit} className="add-document">
        <div className="mb-2">
          <input
            type="text"
            name="title"
            placeholder="title"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.title}
            className={errors.title ? 'invalid' : ''}
          />
          <div className="input__error">{touched.title && errors.title && errors.title}</div>
        </div>
        <div className="flex-1">
          <label htmlFor="documentUpload" className="button secondary">
            Select your document
          </label>
          <input
            ref={documentUpload => (this.documentUpload = documentUpload)}
            type="file"
            name="file"
            id="documentUpload"
            accept="application/pdf"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.file}
            hidden={true}
          />
          <div>{touched.file && errors.file && errors.file}</div>
        </div>

        <button className="button" type="submit" disabled={isSubmitting}>
          Submit
        </button>
      </form>
    );
  };

  private validateForm = (values: any) => {
    const errors: any = {};
    if (!values.title) {
      errors.title = 'You must supply a title';
    }

    if (!values.file || !new RegExp(/.*.pdf$/, 'i').exec(values.file)) {
      errors.file = 'You must supply a valid file';
    }
    return errors;
  };

  private onSubmitForm = async (values: any, { setSubmitting, setErrors }: any) => {
      if (!this.documentUpload || !this.documentUpload.files) {
        throw new Error();
      }

      const event: IAddDocumentEvent = { title: values.title, file: this.documentUpload.files![0] };
      this.props.onSave(event);
      setSubmitting(false);
  };
}

export default MtAddDocument;
