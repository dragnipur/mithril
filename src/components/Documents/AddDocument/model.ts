export interface IAddDocumentEvent {
  title: string;
  file: File;
}
