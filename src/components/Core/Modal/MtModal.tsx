import * as React from 'react';

import { IMtModalProps } from './model';
import './MtModal.css';

class MtDocuments extends React.Component<IMtModalProps, {}> {
  public render() {
    return (
      <div hidden={!this.props.isOpen}>
        <section className="modal">
          <header className="modal__header p-1">
            <span>{this.props.title}</span>{' '}
            <button className="modal__action fab small" onClick={this.props.onClose}>
              ✕
            </button>
          </header>
          <div className="p-1">{this.props.children}</div>
        </section>
        <div className={`backdrop ${this.props.isOpen ? 'open' : ''}`} onClick={this.props.onClose} />
      </div>
    );
  }
}

export default MtDocuments;
