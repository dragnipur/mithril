export interface IMtModalProps {
  isOpen: boolean;
  title: string;
  onClose: () => void;
}
