import * as React from 'react';
import './App.css';

import MtFooter from '../Navigation/Footer/MtFooter';
import MtHeader from '../Navigation/Header/MtHeader';
import MtMain from '../Navigation/Main/MtMain';

class App extends React.Component {
  public render() {
    return (
      <div className="app">
        <MtHeader />
        <MtMain />
        <MtFooter />
      </div>
    );
  }
}

export default App;
