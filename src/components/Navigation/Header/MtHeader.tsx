import * as React from 'react';
import './MtHeader.css';

import logo from '../../../assets/logo.svg';

class MtHeader extends React.Component {
  public render() {
    return (
      <header className="mt-header p-05">
        <img src={logo} className="mr-1"/>
        <h3 className="m-0 p-0">Mithril</h3>
      </header>
    );
  }
}

export default MtHeader;
