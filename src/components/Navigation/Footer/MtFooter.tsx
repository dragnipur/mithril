import * as React from 'react';
import './MtFooter.css';

class MtFooter extends React.Component {
  public render() {
    return <footer className="mt-footer p-05">© 2018</footer>;
  }
}

export default MtFooter;
