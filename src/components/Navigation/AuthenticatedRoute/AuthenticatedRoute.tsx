import * as React from 'react';
import { Redirect } from 'react-router-dom';
import dropboxService from '../../../services/DropboxService';

class AuthenticatedRoute extends React.Component<any, any> {
  public render() {
    const Component = this.props.component;
    return dropboxService.isAuthenticated() ? <Component props={this.props} /> : <Redirect to="/auth" />;
  }
}

export default AuthenticatedRoute;
