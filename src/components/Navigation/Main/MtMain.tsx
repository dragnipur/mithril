import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import MtAuthentication from '../../Authentication/MtAuthentication';
import MtDocuments from '../../Documents/MtDocuments';
import MtEditor from '../../Editor/MtEditor';
import AuthenticatedRoute from '../AuthenticatedRoute/AuthenticatedRoute';

class MtMain extends React.Component {
  public render() {
    return (
      <main>
        <Switch>
          <AuthenticatedRoute exact={true} path="/" component={MtDocuments} />
          <Route path="/auth" component={MtAuthentication} />
          <AuthenticatedRoute path="/editor" component={MtEditor} />
        </Switch>
      </main>
    );
  }
}

export default MtMain;
