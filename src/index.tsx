import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware, createStore, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import 'node_modules/milligram/dist/milligram.css';
import 'node_modules/normalize.css/normalize.css';

import App from './components/App/App';
import './index.css';
import registerServiceWorker from './utils/registerServiceWorker';
import rootReducer from './utils/rootReducer';
import rootSaga from './utils/rootSaga';

const sagaMiddleware = createSagaMiddleware();
const store: Store<any> = createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));
sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
