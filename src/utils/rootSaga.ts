import { all, fork } from 'redux-saga/effects';
import documentSagas from '../components/Documents/reduxSagas';

export default function* rootSaga() {
  yield all([fork(documentSagas)]);
}
