import { combineReducers } from 'redux';
import documentsReducers from '../components/Documents/reduxReducer';

const rootReducer = combineReducers({ documentsReducers });

export default rootReducer;
